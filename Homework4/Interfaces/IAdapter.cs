﻿using Homework4.Model;

namespace Homework4.Interfaces
{
    public interface IAdapter
    {
        void AddUser(Storage storage, User newUser);
        void UpgradeUser(Storage storage, int userId, string name);
        void GetUser(Storage storage, int userId);
        void DeleteUser(Storage storage, int userId);
    }
}