﻿using System.Collections.Generic;
using Homework4.Model;

namespace Homework4.Interfaces
{
    public interface IInterpreter
    {
        void Interpret(List<string> commandString);
        List<Token> Parse(List<string> commandString);
    }
}