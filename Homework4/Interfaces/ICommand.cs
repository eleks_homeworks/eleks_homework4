﻿namespace Homework4.Interfaces
{
    public interface ICommand
    {
        void Execute();
    }
}