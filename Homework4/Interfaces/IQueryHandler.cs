﻿using Homework4.Model;

namespace Homework4.Interfaces
{
    public interface IQueryHandler
    {
        void HandleQuery(ICommand command);
    }
}