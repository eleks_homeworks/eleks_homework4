﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Commands;
using Homework4.Interfaces;

namespace Homework4.Implementations
{
    class QueryHandler : IQueryHandler
    {
        public void HandleQuery(ICommand command)
        {
            if (command is AddCommand)
            {
                AddCommand addCommand = command as AddCommand;
                addCommand.Execute();
            }
            else if (command is UpgradeCommand)
            {
                UpgradeCommand upgradeCommand = command as UpgradeCommand;
                upgradeCommand.Execute();
            }
            else if (command is GetCommand)
            {
                GetCommand getCommand = command as GetCommand;
                getCommand.Execute();
            }
            else if (command is DeleteCommand)
            {
                DeleteCommand deleteCommand = command as DeleteCommand;
                deleteCommand.Execute();
            }
            else
            {
                Console.WriteLine($"Error: unidentified command {command.GetType()}");
            }
        }
    }
}
