﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Commands;
using Homework4.Interfaces;
using Homework4.Model;

namespace Homework4.Implementations
{
    class QueryInterpreter : IInterpreter
    {
        public Storage StorageType { get; set; }

        public QueryInterpreter()
        {
            StorageType = Storage.JSON;
        }

        public void Interpret(List<string> commandString)
        {
            List<Token> query = Parse(commandString);

            switch (query[0].Type)
            {
                case TokenType.Set:
                    if (query[0].Value == "json")
                    {
                        StorageType = Storage.JSON;
                    }
                    else if (query[0].Value == "sql")
                    {
                        StorageType = Storage.SQL;
                    }
                    break;
                case TokenType.Add:
                    if (query[1].Type == TokenType.User)
                    {
                        if (query[2].Type == TokenType.UserId &&
                            query[3].Type == TokenType.FirstName &&
                            query[4].Type == TokenType.LastName)
                        {
                            User newUser = new User(Int32.Parse(query[2].Value), query[3].Value, query[4].Value);
                            AddCommand addCommand = new AddCommand(StorageType, newUser);
                            new QueryHandler().HandleQuery(addCommand);
                        }
                    }
                    break;
                case TokenType.Upgrade:
                    if (query[1].Type == TokenType.User)
                    {
                        if (query[2].Type == TokenType.UserId &&
                            query[3].Type == TokenType.FirstName)
                        {
                            UpgradeCommand upgradeCommand = new UpgradeCommand(StorageType, Int32.Parse(query[2].Value), query[3].Value);
                            new QueryHandler().HandleQuery(upgradeCommand);
                        }
                    }
                    break;
                case TokenType.Get:
                    if (query[1].Type == TokenType.User)
                    {
                        if (query[2].Type == TokenType.UserId)
                        {
                            GetCommand getCommand = new GetCommand(StorageType, Int32.Parse(query[2].Value));
                            new QueryHandler().HandleQuery(getCommand);
                        }
                    }
                    break;
                case TokenType.Delete:
                    if (query[1].Type == TokenType.User)
                    {
                        if (query[2].Type == TokenType.UserId)
                        {
                            DeleteCommand deleteCommand = new DeleteCommand(StorageType, Int32.Parse(query[2].Value));
                            new QueryHandler().HandleQuery(deleteCommand);
                        }
                    }
                    break;
                default:
                    Console.WriteLine("Error: Command undefined");
                    break;
            }
        }

        public List<Token> Parse(List<string> commandString)
        {

            switch (commandString[0].ToLower())
            {
                case "add":
                    {
                        return Add(commandString);
                    }
                case "delete":
                    {
                        return Delete(commandString);
                    }
                case "upgrade":
                    {
                        return Upgrade(commandString);
                    }
                case "get":
                    {
                        return Get(commandString);
                    }

                default: return null;
            }
        }

        private List<Token> Add(List<string> str)
        {
            if (str[1] != "user" || str.Count != 5)
                return null;
            string[] name = str[3].Split(':');
            string[] sname = str[4].Split(':');
            if (name.Length != 2 || sname.Length != 2)
                return null;
            List<Token> Final = new List<Token>(str.Count);
            Final[0] = new Token(TokenType.Add, null);
            Final[1] = new Token(TokenType.User, null);
            Final[2] = new Token(TokenType.UserId, str[2]);
            Final[3] = new Token(TokenType.FirstName, name[1]);
            Final[4] = new Token(TokenType.LastName, sname[1]);
            return Final;
        }

        private List<Token> Delete(List<string> str)
        {
            if (str.Count != 3 || str[1] != "user")
                return null;
            List<Token> Final = new List<Token>(str.Count);
            Final[0] = new Token(TokenType.Delete, null);
            Final[1] = new Token(TokenType.User, null);
            Final[2] = new Token(TokenType.UserId, str[2]);
            return Final;
        }

        private List<Token> Upgrade(List<string> str)
        {
            if (str[1] != "user" || str.Count != 4)
                return null;
            string[] name = str[3].Split(':');
            if (name.Length != 2)
                return null;
            List<Token> Final = new List<Token>(str.Count);
            Final[0] = new Token(TokenType.Upgrade, null);
            Final[1] = new Token(TokenType.User, null);
            Final[2] = new Token(TokenType.UserId, str[2]);
            Final[3] = new Token(TokenType.FirstName, name[1]);
            return Final;
        }

        private List<Token> Get(List<string> str)
        {
            if (str.Count != 3 || str[1] != "user")
                return null;
            List<Token> Final = new List<Token>(str.Count);
            Final[0] = new Token(TokenType.Get, null);
            Final[1] = new Token(TokenType.User, null);
            Final[2] = new Token(TokenType.UserId, str[2]);
            return Final;
        }
    }
}
