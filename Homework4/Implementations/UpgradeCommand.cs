﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Implementations;
using Homework4.Interfaces;
using Homework4.Model;

namespace Homework4.Commands
{
    class UpgradeCommand : ICommand
    {
        public Storage StorageType { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }

        public UpgradeCommand(Storage storageType, int userId, string name)
        {
            StorageType = storageType;
            UserId = userId;
            Name = name;
        }

        public void Execute()
        {
            new StorageAdapter().UpgradeUser(StorageType, UserId, Name);
        }
    }
}
