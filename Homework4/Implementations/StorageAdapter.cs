﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Homework4.Interfaces;
using Homework4.Model;

namespace Homework4.Implementations
{
    class StorageAdapter : IAdapter
    {
        public void AddUser(Storage storage, User newUser)
        {
            if (storage == Storage.JSON)
            {
                Console.WriteLine("Storage type JSON. Added new user:");
                // JSON file
                // Serialize
            }
            else if (storage == Storage.XML)
            {
                Console.WriteLine("Storage type XML. Added new user:");
            }
            else if (storage == Storage.SQL)
            {
                Console.WriteLine("Storage type SQL. Added new user:");
                using (var db = new UserContext())
                {
                    db.Users.Add(newUser);
                    db.SaveChanges();
                }
            }
            else
            {
                Console.WriteLine($"Error: unidentified storage {storage}");
            }
            Console.WriteLine(newUser);
        }

        public void UpgradeUser(Storage storage, int userId, string name)
        {
            if (storage == Storage.JSON)
            {
                Console.WriteLine("Storage type JSON. Upgraded user:");
            }
            else if (storage == Storage.XML)
            {
                Console.WriteLine("Storage type XML. Upgraded user:");
            }
            else if (storage == Storage.SQL)
            {
                Console.WriteLine("Storage type SQL. Upgraded user:");
                using (var db = new UserContext())
                {
                    var original = db.Users.Find(userId);

                    if (original != null)
                    {
                        original.FirstName = name;
                        db.Users.Attach(original);
                        db.Entry(original).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        Console.WriteLine("User with such id doen't exist");
                    }
                }
            }
            else
            {
                Console.WriteLine($"Error: unidentified storage {storage}");
            }
            Console.WriteLine($"Upgraded user {userId}. Set first name to {name}");
        }

        public void GetUser(Storage storage, int userId)
        {
            if (storage == Storage.JSON)
            {
                Console.WriteLine("Storage type JSON. Get user:");
            }
            else if (storage == Storage.XML)
            {
                Console.WriteLine("Storage type XML. Get user:");
            }
            else if (storage == Storage.SQL)
            {
                Console.WriteLine("Storage type SQL. Get user:");
                using (var db = new UserContext())
                {
                    var original = db.Users.Find(userId);
                    if (original != null)
                    {
                        Console.WriteLine(original);
                    }
                    else
                    {
                        Console.WriteLine("User with such id doen't exist");
                    }
                }
            }
            else
            {
                Console.WriteLine($"Error: unidentified storage {storage}");
            }
            Console.WriteLine($"User id = {userId}");
        }

        public void DeleteUser(Storage storage, int userId)
        {
            if (storage == Storage.JSON)
            {
                Console.WriteLine("Storage type JSON. Deleted user:");
            }
            else if (storage == Storage.XML)
            {
                Console.WriteLine("Storage type XML. Deleted user:");
            }
            else if (storage == Storage.SQL)
            {
                Console.WriteLine("Storage type SQL. Deleted user:");
                using (var db = new UserContext())
                {
                    var userToDelete = db.Users.Find(userId);
                    if (userToDelete != null)
                    {
                        db.Users.Remove(userToDelete);
                        db.Entry(userToDelete).State = EntityState.Deleted;
                        db.SaveChanges();
                    }
                }
            }
            else
            {
                Console.WriteLine($"Error: unidentified storage {storage}");
            }
            Console.WriteLine($"User id = {userId}");
        }
    }
}
