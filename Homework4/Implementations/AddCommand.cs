﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Implementations;
using Homework4.Interfaces;
using Homework4.Model;
using Microsoft.Win32;

namespace Homework4.Commands
{
    class AddCommand : ICommand
    {
        public Storage StorageType { get; set; }
        public User NewUser { get; set; }

        public AddCommand(Storage storageType, User newUser)
        {
            StorageType = storageType;
            NewUser = newUser;
        }

        public void Execute()
        {
            new StorageAdapter().AddUser(StorageType, NewUser);
        }
    }
}
