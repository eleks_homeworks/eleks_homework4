﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Implementations;
using Homework4.Interfaces;
using Homework4.Model;

namespace Homework4.Commands
{
    class DeleteCommand : ICommand
    {
        public Storage StorageType { get; set; }
        public int UserId { get; set; }

        public DeleteCommand(Storage storageType, int userId)
        {
            StorageType = storageType;
            UserId = userId;
        }

        public void Execute()
        {
            new StorageAdapter().DeleteUser(StorageType, UserId);
        }
    }
}
