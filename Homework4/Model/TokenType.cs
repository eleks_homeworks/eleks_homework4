﻿namespace Homework4.Model
{
    public enum TokenType
    {
        Add, Upgrade, Get, Delete, Set, User, UserId, FirstName, LastName, Param
    }
}