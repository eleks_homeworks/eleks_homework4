namespace Homework4.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class UserContext : DbContext
    {
        public UserContext()
            : base("name=UserContext")
        {
        }
        public virtual DbSet<User> Users { get; set; }
    }
}