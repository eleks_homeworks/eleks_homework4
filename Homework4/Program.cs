﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework4.Commands;
using Homework4.Implementations;
using Homework4.Model;

namespace Homework4
{
    class Program
    {
        static void Main(string[] args)
        {
            //User user = new User(1, "Roman", "Rudyi");
            //AddCommand addCommand = new AddCommand(Storage.SQL, user);

            //GetCommand getCommand = new GetCommand(Storage.XML, 3);

            //QueryHandler handler = new QueryHandler();

            //handler.HandleQuery(addCommand);
            //handler.HandleQuery(getCommand);

            List<string> query = Console.ReadLine().Split().ToList();

            foreach (var q in query)
            {
                Console.WriteLine(q);
            }

            Console.Read();
        }
    }
}
